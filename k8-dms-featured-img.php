<?php
/*
	Plugin Name: K8 DMS Featured Image
	Plugin URI: http://www.kri8it.com
	Description: Get the featured image for the post or page
	Author: Farn van Zyl
	PageLines: true
	Version: 1.0.1
	Section: true
	Class Name: K8FeatImg
	Filter: component
	Loading: refresh
 
	K8 Bitbucket Plugin URI: https://bitbucket.org/kri8it/k8-dms-featured-img
*/

/**
 * IMPORTANT
 * This tells wordpress to not load the class as DMS will do it later when the main sections API is available.
 * If you want to include PHP earlier like a normal plugin just add it above here.
 */

if( ! class_exists( 'PageLinesSectionFactory' ) )
	return;


class K8FeatImg extends PageLinesSection {
	
	function section_persistent(){
        		
    }
		
	function section_template() {
		
		//Get current post
		if( is_page() || is_single() ):
			
			global $post;
			
			$width = $this->opt( 'k8_featured_image_width', array( 'default' => 610 ) );
			$height = $this->opt( 'k8_featured_image_height', array( 'default' => '' ) );
			
			
			$src = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
			
			// Crop
			$params = array( 'width' => $width, 'crop' => true, 'quality' => 100 );
			if( $height != '' ) $params['height'] = $height;
			$src = bfi_thumb( $src, $params );
			
			
			$featured_image = '<img alt="' . $post->post_title . '" src="' . $src . '" width="' . $width . '" class="k8-featured-img"/>';
			
			echo $featured_image;
			
		else:
			
			echo '<p>This is not a single page or post.</p>';
			
		endif;
		
	}
	function section_opts(){
						
		$opts = array(
			array(
				'type'		=> 'multi',
				'key'		=> 'k8_featured_image_settings',
				'col'		=> 1,
				'opts'		=> array(
				
					array(
						'key'		=> 'k8_featured_image_post_link',						
						'type'		=> 'edit_post',
						'title'		=> __( 'Edit Post Content', 'pagelines' ),
						'label'		=>	__( '<i class="icon icon-edit"></i> Edit Post Info', 'pagelines' ),
						'help'		=> __( 'This section uses WordPress posts. Edit post information using WordPress admin.', 'pagelines' ),
						'classes'	=> 'btn-primary'
					),
					
					array(
						'key'			=> 'k8_featured_image_width',
						'type' 			=> 'template',
						'template'		=> '<label for="k8_featured_image_width">' . __( 'Featured Image Width', 'pagelines' ) . ' <span data-key="k8_featured_image_width" class="pl-help-text btn btn-mini pl-tooltip sync-btn-refresh" title="" data-original-title="Refresh for preview."><i class="icon icon-refresh"></i></span></label>
											<input type="number" class="lstn" id="k8_featured_image_width" name="' . $this->get_the_id() . '[k8_featured_image_width]" value="' . $this->opt('k8_featured_image_width') . '"/> ',
						'label' 		=> __( 'Featured Image Width', 'pagelines' ),
						'default'		=> 610,
						'help' 			=> __( 'Determine the width of the image. Default: 610 *Optional', 'pagelines' )
					),
					array(
						'key'			=> 'k8_featured_image_height',
						'type' 			=> 'template',
						'template'		=> '<label for="k8_featured_image_height">' . __( 'Featured Image Height', 'pagelines' ) . ' <span data-key="k8_featured_image_height" class="pl-help-text btn btn-mini pl-tooltip sync-btn-refresh" title="" data-original-title="Refresh for preview."><i class="icon icon-refresh"></i></span></label>
											<input type="number" class="lstn" id="k8_featured_image_height" name="' . $this->get_the_id() . '[k8_featured_image_height]" value="' . $this->opt('k8_featured_image_height') . '"/> ',
						'label' 		=> __( 'Featured Image Height', 'pagelines' ),
						'default'		=> 205,
						'help' 			=> __( 'Determine the height of the image. Default: 205 *Optional', 'pagelines' )
					)				
				)
			)
		);
		return $opts;		
	}
}